Style guide of expected spellings for commonly-misspelled words:

TREATMENT OF COMPOUND WORDS:
mailing list (NOT mailinglist)
photoreal (NOT photo real)
light bulb (NOT lightbulb)
greyscale (NOT grayscale or gray-scale or grey scale or any other variation)
filmmaker

INITIALISMS AND ACRONYMS:
aka (NOT a.k.a. or AKA or A.K.A.)
