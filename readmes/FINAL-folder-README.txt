This is the FINAL folder, which contains articles ready for layout. These articles contain some markup to indicate particular stylistic treatments. 

If pulling articles from this folder for purposes other than final layout of the magazine, be sure to 1) check for markup 2) attribute the listed author.
